

var fs = require('fs');
var readdirp = require('readdirp');

var path = process.argv[2];
var filter = process.argv[3];
var exclude = "!node_modules,!asar,!npm".split(',');
process.stdout.write(`info: CRAWLER Indexing ${path.toString()} ${filter.toString()} ${exclude.toString()}`.toString());
readdirp(Object.assign({root:path, fileFilter:filter, directoryFilter: exclude }),
    (entry) => {
        process.stdout.write(JSON.stringify({ name: entry.name.split('.').shift(), path: entry.fullPath })+'\n');
    }, (err, res) => {
        process.exit();
    }
);


