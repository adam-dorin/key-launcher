

var spawn = require('child_process').spawn;
var fork = require('child_process').fork;


var IndexModel = require('../../utilities').IndexModel;
var MOST_USED = [];
var locations = {};

var start = (path,logger) => {
    var Data = require('../data/index');
    console.log('started to index most_used');
    var INDEX = [];
    var indexLocation = Data.get('settings').paths.most_used_locations;
   // console.log('start index most_used');
    indexLocation.map(location => {
        // TODO: add exclude param to spawn
        locations[location.root] = spawn('node', [`${__dirname}\\crawler.js`, `${location.root}`, `${location.fileFilter}`]);

        locations[location.root].stdout.on('data', (data) => { //console.log(data.toString());
            if (`${data}`.indexOf('info:') === -1) {

                if (!!`${data}`) {
                    let arr = `${data}`.split('\n').filter(a => !!a).map(as => JSON.parse(as));
                    INDEX.push(...arr);
                    // logger.apply(this, [arr.length, INDEX.length]);
               //  console.log(INDEX);
                }
            } else {
              // console.log([`${data}`]);
            }
        });

        locations[location.root].stderr.on('data', (data) => {
          //  console.log('err:',data);
            return;
        });

        locations[location.root].on('exit', (code, signal) => { 
            // console.log.apply(this, [`exit=>>\ncode: ${code} \n signal:${signal}`]);
            
            var iconExtract = require('../extract-icon');
            // console.log([INDEX.length, INDEX]);
            // Data.set('index','most_used',INDEX);
            // console.log('verify:',Data.get('index'));
            iconExtract(INDEX,(message)=>{
                var parsed = JSON.parse(message);
               console.log('pp;',parsed);
                if (message.error) {
                   //console.log(['error: ', message.error]);
                  //  return;
                }
                if (!!parsed) {

                  // console.log(['parsed: ', parsed]);
                    try {

                        MOST_USED = [...parsed.map(item => new IndexModel(item.name, item.description, item.path, item.icon))];
                    } catch (e) {
                        MOST_USED = [];
                        return;
                    }
                  // console.log([MOST_USED]);
                    if (MOST_USED.length) {
                        Data.set('index','most_used',MOST_USED);

                        console.log('verify:',Data.get('index'));
                       
                    }
                }

            });
            // iconExtract.on('error',(e)=>{
            //     console.log('iconExtract has encountred an error', e);
            // })
            // iconExtract.on('message', (message) => {
            //     var parsed = JSON.parse(message);
            //     console.log(parsed);
            //     if (message.error) {
            //        console.log(['error: ', message.error]);
            //         return;
            //     }
            //     if (!!parsed) {

            //        console.log(['parsed: ', parsed]);
            //         try {

            //             MOST_USED = [...parsed.map(item => new IndexModel(item.name, item.description, item.path, item.icon))];
            //         } catch (e) {
            //             MOST_USED = [];
            //             return;
            //         }
            //        console.log([MOST_USED]);
            //         if (MOST_USED.length) {
            //             Data.set('index','most_used',MOST_USED);

            //             console.log('verify:',Data.get('index'));
                       
            //         }
            //     }
            // });

        });
    });
}

module.exports = start;