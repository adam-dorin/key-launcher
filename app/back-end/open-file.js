var child = require('child_process').exec;
var fs = require('fs');

const start = (path) => {
    fs.writeFile('execute.bat', `call "${path}"`, error => {
        if (error) {
            console.error(error, 'Error in open-file.js@fs.WriteFile');
            // process.exit();
            return;
        }
        child('execute.bat', err => {
            if (err) {
                console.error(err, 'Error in open-file.js@child_process.execFile');
                // process.exit();
                return;
            }

            fs.unlink('execute.bat', e => {
                if (e) {
                    console.error(e, 'Error in open-file.js@fs.unlink')
                    // process.exit();
                    return;
                }
                // process.exit();
                return;
            })

        });
    });
}

module.exports = start;