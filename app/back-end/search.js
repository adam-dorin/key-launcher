var child_process = require('child_process');
var debounce = require('debounce');
const https = require('https');
const cheerio = require('cheerio');

var Data = require('./data/index');
var iconExtractor = require('./extract-icon');
var INDEX = Data.get('index');
var ICONS_FOUND = []; // TODO: Icons found and written to the disk should be written to disk / syncronized in the index

setInterval(() => {
    if (ICONS_FOUND.length) {
        Data.set('index', 'data', ICONS_FOUND);
    }
}, 60000);

const queryFile = function (term = '', paramerters = {}, cb) {
console.log('queryFIle');
    if (!term) {
        return [];
    }
    var no_icons = [];
    var most_used = Data.get('index').most_used.filter(line =>
        line.name.toLowerCase().includes(term) ||
        line.description.toLowerCase().includes(term) ||
        line.path.toLowerCase().includes(term)
    );
    console.log('most_used', INDEX.most_used, most_used.length);
    var res = INDEX.data.filter(line =>
        line.name.toLowerCase().includes(term) ||
        line.description.toLowerCase().includes(term) ||
        line.path.toLowerCase().includes(term)
    );
    console.log(res);
    res = most_used.length > 0 ? most_used.slice(0, (res.length > paramerters.size ? paramerters.size : 15)) : res.slice(0, (res.length > paramerters.size ? paramerters.size : 15));

    res = res.map(result => {
        if (!result.icon) {
            if (ICONS_FOUND.filter(icon => icon.path === result.path).length) {
                result = ICONS_FOUND.filter(icon => icon.path === result.path).shift();
            } else {
                no_icons.push(result);
            }
        }
        return result;
    });
    no_icons.length > 0 ? iconExtractor(no_icons,(message)=>{
        var mes = JSON.parse(message);
        console.log('icons:', mes);
    
        var res = [...results, ...mes].filter(re => !!re.icon).unique();
        ICONS_FOUND.push(...res);
        ICONS_FOUND = ICONS_FOUND.unique();
    
        CALLBACK.apply(this, [res]);
    }) : null;
    return res;
}

const google = (term, cb) => {

    var res = [];
    https.get(`https://www.google.com/search?q=${term}&num=15`, (resp) => {
        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            // console.log(data);
            var $ = cheerio.load(data);
            console.log($('div.g').length);

            $('div.g h3 a').each((i, elem) => {
                console.log(i, $(elem).text(), $(elem).attr('href'));
                res.push({
                    name: $(elem).text(),
                    description: "",
                    path: $(elem).attr('href'),
                    icon: "",
                    type: "url"
                });

            });
            cb.apply(this, [res])
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}

Array.prototype.unique =  // extend the array prototype
    [].unique ||              // if it doesn't exist
    function (
        a
    ) {
        return function () {     // with a function that
            return this.filter(a) // filters by the cached function
        }
    }(
        function (a, b, c) {       // which
            return c.indexOf(     // finds out whether the array contains
                a,                  // the item
                b + 1               // after the current index
            ) < 0                 // and returns false if it does.
        }
    );
var CALLBACK = null;
var results = [];
var query = function (term, paramerters, cb) {
    if (paramerters.type === 'file') {
        CALLBACK = cb;
        results = queryFile(term, paramerters, cb);

        if (!!results.length) {
            cb.apply(this, [results]);
        } else {
            cb.apply(this, [[]])
        }
    }

    if(paramerters.type === 'google'){
        google(term,cb);
    }

}
// iconExtractor.on('message', (message) => {

//     var mes = JSON.parse(message);
//     console.log('icons:', mes);

//     var res = [...results, ...mes].filter(re => !!re.icon).unique();
//     ICONS_FOUND.push(...res);
//     ICONS_FOUND = ICONS_FOUND.unique();

//     CALLBACK.apply(this, [res]);
// })

module.exports.query = query;