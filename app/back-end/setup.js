var fs = require('fs');

const start = (path, base_path ,callback )=>{
console.log(path,base_path,callback);
var files ={ 
    settings:{ 
    "paths": { 
        "most_used_locations": [ 
            { 
                "root": process.env.APPDATA+"\\Microsoft\\Windows\\Start Menu", 
                "fileFilter": "*.lnk" 
            } 
        ], 
        "index": [ 
            { 
                "root": "C:\\", 
                "fileFilter": "*.exe" 
            } 
        ] 
    }, 
    "ignore": [ 
        "!node_modules/", 
        "!.asar", 
        "!npm/" 
    ] 
} ,
index: { 
    "data":[], 
    "most_used":[] 
}

};

var filesList = Object.keys(files);

filesList.map((file,index)=> {
    
    fs.writeFile(`${process.env.APPDATA}\\KeyLauncher\\${file}.json`, JSON.stringify(files[file]),'utf-8', (err) => {
        if (err) throw err;
        console.log(`The ${file} file have been saved!`);
        // process.exit();
        if(index===filesList.length-1){
            console.log('app.setup.callback',callback)
            !!callback?callback.apply(this,[]):null;
        }
        return;
    });
});


}

module.exports = start;