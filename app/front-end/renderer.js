/**
 * @description functions helping out with the opening windows applications and getting infor from .lnk file 
 */

const backend = require('../back-end/index');
const { ipcRenderer } = require('electron');
const os = require('os');
const opn = require('opn');

const openApp = (path, callback) => {

    if (!path || !callback) {
        !path ? console.error('Error: path is mandatory in renderer.js->openApp') : null;
        !callback ? console.error('Error: callback is mandatory in renderer.js->openApp') : null;
    }

    backend.openFile(path);
}

const query = (term, param, cb) => {
    backend.search.query(term, param, cb);
}

const dismiss = () => {
    ipcRenderer.send('dismiss');
}

const resize = (x, y) => {
    ipcRenderer.send('resize', x, y);
}

const osVersion = () => {
    var osLabel = '';
    var osVersionNo = os.release();
    if (osVersionNo.includes('6.1')) {
        return 'win7';
    }
    if (osVersionNo.includes('6.2') || osVersionNo.includes('6.3')) {
        return 'win8';
    }
    if (osVersionNo.includes('10.0')) {
        return 'win10';
    }

}

const webOpen = link => {
    // Opens the link in the default web browser;
    opn(`https://google.com${link}`);
}

const onFocus = callback => {
    ipcRenderer.on('focus',()=>{
        callback.apply(this,[]);
    })
}

// Add most used location on startup if setup has not already run;

backend.startUp((data, path) => {

    if (data === 'settingsFound') {
        console.log('backendCrawling')
        backend.crawling.mostUsed(path, (log) => {
            if (log == 'Saved most used locations') {
                console.log('done most used')
                // backend.setup( path );
            }
        });
    }
    if (data === 'runSetup') {
        console.log(`running setup... now? ${data} ${path}`);
        backend.setup(path, process.env.APPDATA, () => {
            backend.crawling.mostUsed(path, (log) => {
                if (log == 'Saved most used locations') {
                    console.log('done most used')
                    // backend.setup( path );
                }
            });
        });
    }
});

module.exports.openApp = openApp;
module.exports.query = query;
module.exports.windowDismiss = dismiss;
module.exports.resize = resize;
module.exports.osVersion = osVersion;
module.exports.webOpen = webOpen;
module.exports.onFocus = onFocus;
