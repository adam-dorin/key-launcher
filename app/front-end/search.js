const { BrowserWindow } = require('electron').remote
const path = require('path')


const renderer = require('../front-end/renderer');
// const searchModule = require('./search');
const debounce = require('debounce');

jQuery(document).ready(function () {

    renderer.onFocus(() => {
        $('#searchInput').focus();
        $('#searchInput').val('');
        console.log('focus');
    });

    var App = new Vue({
        el: '#vueApp',
        data: {
            animated: renderer.osVersion() === 'win10',
            minWindowHeight: 70,
            Width: 800,
            maxWH: 950,

            selectSound: document.querySelector('audio.select'),
            searchSwitchSound: document.querySelector('audio.switch-search-icon'),

            SearchType: {
                selected: 0,
                values: [
                    'file',
                    'google'
                ]
            },

            ActiveSearch: 'file',
            results: []
        },
        methods: {

            openApp: function (path) {
                if (this.ActiveSearch === 'file') {
                    renderer.openApp(path, err => {
                        if (err)
                            return console.error(err);
                    });
                }

                if (this.ActiveSearch === 'google') {
                    // renderer.webOpen
                   // console.log(renderer);
                    renderer.webOpen(path);
                }
            },
            selectSearchItem: function (direction) {
            
                var updated = false;
                if (!this.results.filter(result => result.selected).length && direction === 'up') {
                    return;
                }
                if (!this.results.filter(result => result.selected).length && direction === 'down') {
                    this.results[0].selected = true;
                    return;
                }
                this.results = this.results.map((result, index) => {
                    if (result.selected && !updated) {
                        result.selected = false;
                        let i = direction === 'down' ? ((index + 1) >= (this.results.length - 1) ? (this.results.length - 1) : (index + 1)) : ((index - 1) <= 0 ? 0 : (index - 1))
                        this.results[i].selected = true;
                        updated = true;
                    }
                    return result;
                })
            },
            switchSearch: function (direction) {
                
                var selectionHappened = false;
               
                this.SearchType.values.map((value,i)=>{
                    if(i === this.SearchType.selected && !selectionHappened ) {
                        
                        let index = this.SearchType.selected; 
                        if(direction === 'down'){
                            index++;
                            if(index >=this.SearchType.values.length-1){
                                index = this.SearchType.values.length - 1;
                            }
                        } else {
                            index--;
                            if(index <= 0){
                                index = 0
                            }
                        }
                        this.SearchType.selected = index;
                        this.ActiveSearch = this.SearchType.values[index]; 
                        selectionHappened = true;
                    }         
                })
            },
            search: function ($event) {
                console.log($event);
                return debounce(this._search, 300)($event);
            },
            _search: function ($event) {

               // console.log($event.key, $event);
                //console.log($event.key);
                if ($event.key === 'Escape') {
                    renderer.windowDismiss();
                    return;
                }

                if ($event.key === 'Enter') {
                    var selectedItem = null;
                    try{

                        selectedItem = this.results.filter(result=>result.selected).shift();
                        this.openApp(selectedItem.path);
                    }catch(e) {
                        console.log(e);
                    }
                    if(!selectedItem && this.ActiveSearch === 'google'){
                        this.openApp(`/search?q=${$event.target.value}`)
                    }
                    return;
                }

                if ($event.key === 'ArrowUp') {

                    if ($event.altKey) {
                        // selectSound.play();
                        this.switchSearch('up');
                    } else {
                        this.selectSearchItem('up');
                        // searchSwitchSound.play();
                    }
                    return;
                }

                if ($event.key === 'ArrowDown') {

                    if ($event.altKey) {
                        // selectSound.play();
                        this.switchSearch('down');
                    } else {
                        // searchSwitchSound.play();
                        this.selectSearchItem('down');
                    }
                    return;
                }
                var context = this;
                if (!$event.ctrlKey && $event.key.length === 1 && /[a-z0-9]/i.test($event.key) || $event.key === 'Backspace') {
                    renderer.query($event.target.value, { size: 10, type: this.ActiveSearch }, debounce(function (res) {
                        // renderSearch(results, $event.target.value);
                        context.$data.results = res;
                        if(context.$data.results.length>0 && context.$data.ActiveSearch === 'file'){
                            context.$data.results[0].selected = true;
                        }
                        setTimeout(() => {
                            var winH = $('.input-group.input-group-lg').height();
                            winH += parseInt($('.input-group.input-group-lg').css('padding-top').split('px').shift())*2 ;
                            winH += $('.list-group').height();
                            
                            console.log('timeout: ', context.$data.Width, winH);
                            renderer.resize(context.$data.Width, winH || context.$data.minWindowHeight);
                        }, 50);
                    }, 350));
                }

            }
        }

    });
});
