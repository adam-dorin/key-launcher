const { app, BrowserWindow, globalShortcut, ipcMain, autoUpdater, dialog } = require('electron')

// // Handle setupevents as quickly as possible

// const setupEvents = require('./installers/setupEvents');
// if (setupEvents.handleSquirrelEvent()) {
//   // squirrel event handled and app will exit in 1000ms, so don't do anything else
//   return;
// }

const handleSquirrelEvent = () => {
  if (process.argv.length === 1) {
    return false;
  }

  const squirrelEvent = process.argv[1];
  switch (squirrelEvent) {
    case '--squirrel-install':
    case '--squirrel-updated':
    case '--squirrel-uninstall':
      setTimeout(app.quit, 1000);
      return true;

    case '--squirrel-obsolete':
      app.quit();
      return true;
  }
}

if (handleSquirrelEvent()) {
  // squirrel event handled and app will exit in 1000ms, so don't do anything else
  return;
}
// https://gitlab.com/adam-dorin/-/jobs/artifacts/master/download?job=coverage
// Modules to control application life and create native browser window


// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let settingsWindow;


const squirrelUrl = "localhost:8080";

const startAutoUpdater = (squirrelUrl) => {
  // The Squirrel application will watch the provided URL
  autoUpdater.setFeedURL(`${squirrelUrl}/`);

  // Display a success message on successful update
  autoUpdater.addListener("update-downloaded", (event, releaseNotes, releaseName) => {
    dialog.showMessageBox({"message": `Event ${event} `});
    dialog.showMessageBox({"message": `Event ${releaseNotes} `});
    dialog.showMessageBox({"message": `The release ${releaseName} has been downloaded`});
  });

  // Display an error message on update error
  autoUpdater.addListener("error", (error) => {
    dialog.showMessageBox({"message": "Auto updater error: " + error});
  });

  // tell squirrel to check for updates
  autoUpdater.checkForUpdates();
}

function createSettingsWindow() {
  settingsWindow = new BrowserWindow({
    show: false,
    frame:false,
    width: 1000,
    height: 900,
    resizable: true,
    center:true,
    icon:'assets/icons/win/icon.ico',
    'web-preferences': { 'plugins': true }
  });

  settingsWindow.loadFile('views/settings.html');

 
  // Emitted when the window is closed.
  settingsWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    settingsWindow = null;
    globalShortcut.unregister('CommandOrControl+Alt+Space');
  });

  globalShortcut.register('CommandOrControl+Alt+Space', () => {
    if (settingsWindow === null) {
      createSettingsWindow();
    }else{
      settingsWindow.show();
      settingsWindow.webContents.openDevTools();
    }
  });

}

function createMainWindow() {
  // Create the browser windows.
  mainWindow = new BrowserWindow({
    show: false,
    width: 800,
    height: 70,
    frame: false,
    transparent: true,
    alwaysOnTop: true,
    skipTaskbar: true,
    resizable: true,
    y: 20,
    x: 500,
    'web-preferences': { 'plugins': true }
  });

  // load window files.
  mainWindow.loadFile('views/index.html');


  // Open the DevTools.
  //mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
  mainWindow.on('blur', function () { // this needs to be disabled in dev
    mainWindow.hide();
  });
  
  globalShortcut.register('Alt+Space', () => {
    
    mainWindow.show();
    mainWindow.setSize(800, 70, true);
    mainWindow.webContents.send('focus', true);
  });

  ipcMain.on('dismiss', () => { mainWindow.hide(); });
  ipcMain.on('resize', (e, x, y) => {
    // console.log(x, y);
    mainWindow.setSize(x, y, true);
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', ()=>{
  createMainWindow();
  createSettingsWindow();
  // if (process.env.NODE_ENV !== 'production') {
    // require('vue-devtools').install()
    
  // }

  if (process.env.NODE_ENV !== "dev") startAutoUpdater(squirrelUrl);
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
    // Unregister all shortcuts.
    globalShortcut.unregisterAll();
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createMainWindow();
    
  }

  if (settingsWindow === null) {
    createSettingsWindow();
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
// module.exports.window = mainWindow;
