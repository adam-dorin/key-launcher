
var randomString = () => {
    return  Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);;
}

var logger = (debug) => {
    return debug === true ? console.log : process.stdout.write;
}

var IndexModel = function (name, description, path, icon) {

    this.name = name || "";
    this.description = description || "";
    this.path = path || "";
    this.icon = icon || "";
}

module.exports.randomString = randomString;
module.exports.logger = logger;
module.exports.IndexModel = IndexModel;